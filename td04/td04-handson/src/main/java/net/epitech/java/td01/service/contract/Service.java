package net.epitech.java.td01.service.contract;

import java.util.Collection;

import org.joda.time.DateTime;
import org.joda.time.Duration;

import net.epitech.java.td01.model.Course;
import net.epitech.java.td01.model.Teacher;
import net.epitech.java.td01.service.impl.Pair;

//TODO: interface must be throughly documented
public interface Service {
	public Collection<Course> getCourses();

	public Collection<Teacher> getTeachers();

	public Collection<Pair<DateTime, DateTime>> getAvaibleTimeSlot(Duration d);

	public Course addCourse(String name, DateTime date, Duration duration,
			Teacher t) throws Exception;

	public void deleteCourse(Integer id) throws Exception;

	public Teacher addTeacher(String name, String mail) throws Exception;

	public void deleteTeacher(Integer id) throws Exception;
}