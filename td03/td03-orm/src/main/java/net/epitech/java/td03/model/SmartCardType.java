package net.epitech.java.td03.model;

import javax.annotation.Generated;

/**
 * SmartCardType is a Querydsl bean type
 */
@Generated("com.mysema.query.codegen.BeanSerializer")
public class SmartCardType {

    private Integer id;

    private String typeName;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getTypeName() {
        return typeName;
    }

    public void setTypeName(String typeName) {
        this.typeName = typeName;
    }

}

