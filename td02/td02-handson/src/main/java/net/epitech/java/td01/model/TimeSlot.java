package net.epitech.java.td01.model;
import java.sql.Time;

public class TimeSlot {

	private Day day;
	private Time begin;
	private Time end;
	
	public Day getDay() {
		return day;
	}
	public void setDay(Day day) {
		this.day = day;
	}
	public Time getBegin() {
		return begin;
	}
	public void setBegin(Time begin) {
		this.begin = begin;
	}
	public Time getEnd() {
		return end;
	}
	public void setEnd(Time end) {
		this.end = end;
	}
}
