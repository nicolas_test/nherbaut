<%@ include file="menu-header.jsp"%>


<div class="hero-unit" teacherId="<c:out value="${teacher.id}"></c:out>">
	<h1>
		<c:out value="${teacher.name}"></c:out>
	</h1>
	<p>
		<c:out value="${teacher.mail}"></c:out>
	</p>
</div>

<table class="table">
	<tr>
		<th>Nom</th>
		<th>Dur�e</th>
	</tr>
	<c:forEach var="cours" items="${courses}">
		<tr courseId="<c:out value="${cours.id}"/>">

			<td><c:out value="${cours.name}"></c:out></td>

			<td><c:out value="${cours.duration}"></c:out></td>

			<td><button class="delete-button">X</button></td>
		</tr>
	</c:forEach>
</table>

<script src="http://code.jquery.com/jquery-1.11.0.min.js"></script>
<script src="http://code.jquery.com/jquery-migrate-1.2.1.min.js"></script>

<script type="text/javascript">
	jQuery(document).ready(
			function() {
				jQuery(".delete-button").click(
						function() {
							var courseId = $(this).parent().parent().attr(
									"courseId");
							var teacherId = $(".hero-unit").attr("teacherId");

							jQuery.get(
									"<c:out value="${webappRoot}" />/detach-course-from-teacher?teacherId="
											+ teacherId + "&courseId="
											+ courseId, function(data) {
										jQuery(
												"tr[courseId=\"" + courseId
														+ "\"]").each(
												function() {
													jQuery(this).hide();
												});

									});
						});
			});
</script>

<%@ include file="menu-footer.jsp"%>
