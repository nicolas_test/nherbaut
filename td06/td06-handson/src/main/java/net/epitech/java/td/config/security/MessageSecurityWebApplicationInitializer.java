package net.epitech.java.td.config.security;

import org.springframework.security.web.context.AbstractSecurityWebApplicationInitializer;

/**
 * initialize security.	
 * @author nicolas
 *
 */
public class MessageSecurityWebApplicationInitializer extends
		AbstractSecurityWebApplicationInitializer {
}
