package net.epitech.java.td.service;

import java.security.Principal;
import java.util.Collection;

import org.springframework.security.access.prepost.PreAuthorize;

import net.epitech.java.td.domain.Course;
import net.epitech.java.td.domain.Teacher;
import net.epitech.java.td.exception.FailedToParseDayInWeekException;
import net.epitech.java.td.exception.FailedToParseMailException;
import net.epitech.java.td.exception.InvalidPasswordException;
import net.epitech.java.td.exception.NoSuchCourseException;
import net.epitech.java.td.exception.NoSuchTeacherException;

/**
 * Main service for teacher/course business.
 * @author nicolas
 *
 */
public interface EpitechService {

	public Collection<Course> getCourses();

	@PreAuthorize("hasRole('ADMIN')")
	public Collection<Teacher> getTeacherWithNoCourses();

	public Collection<Teacher> getTeachers();

	// public Collection<Pair<DateTime, DateTime>> getAvaibleTimeSlot(Duration
	// d);

	@PreAuthorize("hasAnyRole('ADMIN','TEACHER')")
	public Course addCourse(String name, String date, Integer duration,
			String teacherMail) throws FailedToParseDayInWeekException;

	@PreAuthorize("hasRole('ADMIN')")
	public void deleteCourse(Integer id) throws NoSuchCourseException;

	@PreAuthorize("hasRole('ADMIN')")
	public Teacher addTeacher(String name, String mail, String password)
			throws FailedToParseMailException;

	@PreAuthorize("hasRole('ADMIN')")
	public void deleteTeacher(Integer id) throws Exception;

	@PreAuthorize("hasRole('ADMIN')")
	public void addTeacherWithCourse(String name, String email,
			String password, Integer cours) throws FailedToParseMailException;

	public Course getCourseById(Integer courseId) throws NoSuchCourseException;

	@PreAuthorize("hasRole('ADMIN')")
	public void detachCourseFromTeacher(Integer courseId, Integer teacherId)
			throws NoSuchTeacherException, NoSuchCourseException;

	public Teacher getTeacher(Integer idTeacher);

	@PreAuthorize("isAuthenticated()")
	public void changePassword(String name, String newPassword,
			String oldPassword) throws NoSuchTeacherException, InvalidPasswordException;

}