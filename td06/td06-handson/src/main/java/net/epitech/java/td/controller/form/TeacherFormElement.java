package net.epitech.java.td.controller.form;

import org.hibernate.validator.constraints.Email;
import org.hibernate.validator.constraints.NotEmpty;
/**
 * Form bean used to create a teacher.
 * @author nicolas
 *
 */
public class TeacherFormElement {

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getCours() {
		return cours;
	}

	public void setCours(String cours) {
		this.cours = cours;
	}

	@NotEmpty
	String name;
	@NotEmpty
	@Email
	String email;

	String cours;
	
	
	@NotEmpty
	String password;
}
