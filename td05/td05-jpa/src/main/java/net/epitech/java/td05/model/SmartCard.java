package net.epitech.java.td05.model;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the SmartCard database table.
 * 
 */
@Entity
@NamedQuery(name="SmartCard.findAll", query="SELECT s FROM SmartCard s")
public class SmartCard implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	private int id;

	//bi-directional many-to-one association to Person
	@ManyToOne
	@JoinColumn(name="holder")
	private Person person;

	//bi-directional many-to-one association to SmartCardType
	@ManyToOne
	@JoinColumn(name="type")
	private SmartCardType smartCardType;

	public SmartCard() {
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Person getPerson() {
		return this.person;
	}

	public void setPerson(Person person) {
		this.person = person;
	}

	public SmartCardType getSmartCardType() {
		return this.smartCardType;
	}

	public void setSmartCardType(SmartCardType smartCardType) {
		this.smartCardType = smartCardType;
	}

}